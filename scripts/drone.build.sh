#!/bin/sh

#cd /drone/src/pwcalc-runner
REPO="gitlab.com/theneedyguy/pwcalc"

go get github.com/grandcat/zeroconf
go get ${REPO}/cmd/pwcalc-runner
go build -a -v -installsuffix cgo -o release/${GOOS}/${GOARCH}/pwcalc ${REPO}/cmd/pwcalc-runner
#go build -a -installsuffix cgo -o pwcalc .
