package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/grandcat/zeroconf"
)

type Response struct {
	Password string `json:"password"`
	Metadata struct {
		RunnerHostname string `json:"runner_hostname"`
		ProcessingTime string `json:"elapsed_time"`
	} `json:"metadata"`
}

func paramHandler(res http.ResponseWriter, req *http.Request) {
	start := time.Now()
	if req.Method != "POST" {
		var hostname, _ = os.Hostname()
		var resp Response
		resp.Password = "ERROR"
		resp.Metadata.RunnerHostname = hostname
		//a := &Response{"ERROR"}
		out, err := json.Marshal(resp)
		if err != nil {
			panic(err)
		}
		io.WriteString(res, string(out))

	} else {
		alias := req.FormValue("alias")
		secret := req.FormValue("secret")
		password := combineStrings(alias, secret)

		//a := &response{createB64(createSHA(password))[0:16]}
		var hostname, _ = os.Hostname()
		var resp Response
		resp.Password = createB64(createSHA(password))[0:16]
		t := time.Since(start)
		resp.Metadata.RunnerHostname = hostname
		resp.Metadata.ProcessingTime = t.String()
		out, err := json.Marshal(resp)
		//a := &response{
		//	Password: createB64(createSHA(password))[0:16],
		//Metadata: {Metadata.RunnerHostname: os.Hostname()},
		//}
		//out, err := json.Marshal(a)
		if err != nil {
			panic(err)
		}

		io.WriteString(res, string(out))
	}
}

func main() {

	// Extra information about our service
	meta := []string{"version=1.0.0"}

	service, err := zeroconf.Register(
		"pwcalcsrv",    // service instance name
		"_pwcalc._tcp", // service type and protocl
		"local.",       // service domain
		1099,           // service port
		meta,           // service metadata
		nil,            // register on all network interfaces
	)

	if err != nil {
		log.Fatal(err)
	}

	defer service.Shutdown()

	fmt.Println("pwcalc-runner listening on port 1099")
	http.HandleFunc("/calc", func(res http.ResponseWriter, req *http.Request) {
		res.Header().Set("Content-Type", "application/json")
		paramHandler(res, req)
	})
	http.ListenAndServe(":1099", nil)
}
