package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"

	"github.com/grandcat/zeroconf"
)

var alias string
var secret string

// Response struct used to parse json response
type Response struct {
	Password string `json:"password"`
	Metadata struct {
		RunnerHostname string `json:"runner_hostname"`
		ProcessingTime string `json:"elapsed_time"`
	} `json:"metadata"`
}

func serviceCall(ip string, port int) {
	apiURL := fmt.Sprintf("http://%v:%v/calc", ip, port)

	fmt.Println("Making call to", apiURL)
	//resp, err := http.Get(url)

	resp, err := http.PostForm(apiURL, url.Values{
		"alias":  {alias},
		"secret": {secret},
	})

	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	data, _ := ioutil.ReadAll(resp.Body)
	returnValue := Response{}
	jsonData := json.Unmarshal(data, &returnValue)
	if jsonData != nil {
		log.Fatal(err)
	}
	fmt.Printf("Got response: %s\n", data)
	fmt.Printf("%s\n", returnValue.Password)
	os.Exit(0)
}

func init() {
	flag.StringVar(&alias, "alias", "", "Alias value")
	flag.StringVar(&secret, "secret", "", "Secret value")
}

func main() {
	flag.Parse()
	resolver, err := zeroconf.NewResolver(nil)
	if err != nil {
		log.Fatal(err)
	}

	// Channel to receive discovered service entries
	entries := make(chan *zeroconf.ServiceEntry)

	go func(results <-chan *zeroconf.ServiceEntry) {
		for entry := range results {
			fmt.Println("Found service:", entry.ServiceInstanceName(), entry.Text)
			serviceCall(entry.AddrIPv4[0].String(), entry.Port)
		}
	}(entries)

	ctx := context.Background()

	err = resolver.Browse(ctx, "_pwcalc._tcp", "local.", entries)
	if err != nil {
		log.Fatalln("Failed to browse:", err.Error())
	}

	<-ctx.Done()
}
