#!/usr/bin/env python3
from flask import Flask, Response
from flask import request
from flask import render_template
import hashlib
import base64
import re
import uuid
import requests
import json
import os

app = Flask(__name__)

@app.route('/')
def my_form():
    return render_template("index.html")

@app.route('/', methods=['POST'])
def my_form_post():
    if request.form['alias'] != "" and request.form['secret'] != "":
        alias = request.form['alias']
        secret = request.form['secret']
        pwcalc_runner = os.environ["PWCALC_RUNNER_CONTAINER"]
        print(pwcalc_runner)
        url = "http://"+pwcalc_runner+"/calc"
        params = {'alias': alias, 'secret': secret}
        r = requests.post(url = url, params = params)
        return render_template("index.html", password = json.loads(r.text)["password"])
    else:
        return render_template("index.html", password = "Not able to calculate with empty values.")

@app.route('/api', methods=["POST"])
def api():
    json_data = request.get_json(force=True)
    if json_data == None:
        return "No valid json sent. Escape the double-quotes."
    try:
        alias = json_data["alias"]
        secret = json_data["secret"]
    except:
        return 'Invalid json. Send {"alias": "<your alias>", "secret": "<your secret>"}'
    # Get container name from env
    pwcalc_runner = os.environ["PWCALC_RUNNER_CONTAINER"]
    url = "http://"+pwcalc_runner+"/calc"
    # Send parameters to URL
    params = {'alias': alias, 'secret': secret}
    r = requests.post(url = url, params = params)
    # Make a response with the right header
    resp = Response(json.dumps(json.loads(r.text)))
    resp.headers['Content-Type'] = 'application/json'
    return resp

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=3333)

